#include "led_status.h"
int status = NO_LED;
/*
 * Set the led status.
 * @param int led the status
 */
void setStatus(int led){
  // the same status.
  if (led==status){
    return;
  }
  wakeUp();
  
  if (led==LED_ERROR){
    digitalWrite(LED_OK,HIGH);
    digitalWrite(LED_NOK,HIGH);
    digitalWrite(LED_ALARM,HIGH);
  }
  
  if (status==LED_ERROR){
    digitalWrite(LED_OK,LOW);
    digitalWrite(LED_NOK,LOW);
    digitalWrite(LED_ALARM,LOW);
  }
  
  if (status!=NO_LED){
    digitalWrite(status,LOW);
  }

  digitalWrite(led,HIGH);
  status = led;
}
/**
 * Retrieve the status
 */
int getStatus(){
  return status;
}

