#ifndef consumption_H
#define consumption_H

#include <Arduino.h>
#include "const.h"
/**
 * Listen to voltage difference.
 */
void listenEnviroment(float voltage);
/**
 * It tries to enable the power saving.
 */
void pleaseSleep();
/**
 * It  disable the power saving.
 */
void wakeUp();
/**
 * Update the brightness of the lcd.
 */
void lcd_update();


#endif
