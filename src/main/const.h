#ifndef const_H
#define const_H

typedef void (* onButtonRelease) ();

// ---------------------
// ---- ALL PINS -------
// ---------------------

// LED 
#define NO_LED 0  // a virtual pin. s
#define LED_NOK 2
#define LED_ALARM 7
#define LED_OK 5
#define LED_ERROR 255 // a virtual pin. 

// BUTTON ANALOG INPUT 
#define BUTTON_UP A5
#define BUTTON_DOWN A4
#define BUTTON_SENSITIVITY A3


// LCD  PIN
#define LCD_REGISTER 4 
#define LCD_ENABLE 6
#define LCD_DATA1 10
#define LCD_DATA2 11 
#define LCD_DATA3 12
#define LCD_DATA4 13

// LCD LIGHT AND CONSTRAST DIGITAL.
#define LCD_BRIGHTNESS 3
#define LCD_CONTRAST 9

// VOLTAGE PIN 
#define VOLTAGE_ENABLE 8
#define VOLTAGE_READ A0


// -------------------------------------------
// ---------- APPLICATION CONFIG -------------
// -------------------------------------------
#define APP_NAME "BatteryTester"


// SENSITIVITY POSSIBILITY
#define SENSITIVITY_ONE_PERCENT 0.01
#define SENSITIVITY_FIVE_PERCENT 0.05
// SENSITIVITY DEFAULT STATE
#define DEFAULT_SENSITIVITY SENSITIVITY_ONE_PERCENT

// LCD DEFAULT STATE
#define LCD_DEFAULT_CONTRAST 127
#define LCD_DEFAULT_BRIGHTNESS 127

// VOLTAGE
#define VOLTAGE_SAMPLE_SIZE 20
#define VOLTAGE_SAMPLE_RATE 10
#define PARTIAL_MEASURE_RATE 250

// VOLTAGE LEVELS
#define VOLTAGE_LIMIT_SIZE 5
#define VOLTAGE_DEFAULT_INDEX 0 // default limit voltage.


// VOLTAGE FRONT END.
#define VOLTAGE_CURRENT_STRING "N" 
#define VOLTAGE_LIMIT_STRING   "L" 
#define VOLTAGE_SYMBOL "V"

// SENSITIVITY FRONT END
#define SENSITIVITY_STRING "S"

// VOLTAGE DETAILS
#define VOLTAGE_EPSILON 0.005
#define VOLTAGE_SWITCH_OFF_MAX_VOLTAGE 0.05
#define VOLTAGE_CIRCUIT_MULTIPLIER 1 // for possible tension partition.

// POWER SAVING STEP
#define INACTIVE_DECREASE_STEP 10 // FROM 0 TO 100.
#define POWER_SAVING_MAX 250
#define POWER_SAVING_MIN 150
#define MAX_SILENCE_USAGE_MILLISECONDS 20000
#define MAX_SILENCE_USAGE MAX_SILENCE_USAGE_MILLISECONDS/PARTIAL_MEASURE_RATE // greater than zero.

// CONFIG 
#define CONFIG_FIRST_TIME_BYTE 1
#define CONFIG_VOLTAGE_LIMIT_INDEX 1
#define CONFIG_SENSITIVITY_INDEX 2

// -------------------------------------------
// ---------- AUTOTEST CONFIG ----------------
// -------------------------------------------
#define AUTOTEST_LED_DELAY 1000 // 1 sec for waiting the led.

#define AUTOTEST_MESSAGE "Test phase"


// -------------------------------------------
// ---------- AUTOTEST STATE------------------
// -------------------------------------------
#define AUTOTEST_OK 0
#define AUTOTEST_ERROR_BUTTON_UP 1
#define AUTOTEST_ERROR_BUTTON_DOWN 2
#define AUTOTEST_ERROR_BUTTON_SENSITIVITY 3
#define AUTOTEST_ERROR_VOLTAGE_ZERO 4
// -------------------------------------------
// ---------- AUTOTEST STATE FRONT END--------
// -------------------------------------------
#define AUTOTEST_OK_STRING "Autotest ok"
#define AUTOTEST_ERROR_BUTTON_UP_STRING "AT butt up"
#define AUTOTEST_ERROR_BUTTON_DOWN_STRING "AT butt down"
#define AUTOTEST_ERROR_BUTTON_SENSITIVITY_STRING "AT butt sens"
#define AUTOTEST_ERROR_VOLTAGE_ZERO_STRING "AT volt zero"



#endif
