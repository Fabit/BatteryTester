#include "power_saving.h"
// LAST VOLTAGE FOR CHECKING VOLTAGE DIFFERENCE
float last_voltage = -1;
// POWER SAVING PERCENTAGE.
float current_usage = 100;
// SILENCE USAGE TICK.
int   silence_usage = 0;
// CONVERT FROM PERCENTAGE TO THE RIGHT SCALE.
#define POWER_SAMPLE (POWER_SAVING_MAX-POWER_SAVING_MIN)/100

/**
 * It allows to disable the power saving.
 */
void listenEnviroment(float voltage){
  if (last_voltage==-1){
    last_voltage = voltage;
    return;
  }
  if (abs(last_voltage - voltage) >0.01){ // even if increase or decrease check difference
    wakeUp();
    
  }
  last_voltage = voltage;
}

void decreaseUsage(float decrease){
  if (current_usage<0){
    return;
  }
  current_usage -= decrease;
}
/**
 * It  disable the power saving.
 */
void pleaseSleep(){
  // CHECK MAXIMUM SILENCE USAGE.
  if (silence_usage==MAX_SILENCE_USAGE){
    // IF IT IS DECREASE USAGE.
      decreaseUsage(INACTIVE_DECREASE_STEP);
      lcd_update();
  }
  else{
    // INCREASE SILENCE.
      silence_usage++;
  }
  
}
/**
 * Force to disable power_saving.
 */
void wakeUp(){
  
  current_usage = 100;
  silence_usage = 0;
  lcd_update();
}

/**
 * Apply power saving.
 */
void lcd_update(){
  float usage = (current_usage*POWER_SAMPLE)+POWER_SAVING_MIN;
  analogWrite(LCD_BRIGHTNESS,usage);
}


