#include "config.h"

bool firstTime = false;



/**
 * save default value on EEPROM.
 */
void onFirstTimeSave(){
  EEPROM.put(CONFIG_VOLTAGE_LIMIT_INDEX,(byte)VOLTAGE_DEFAULT_INDEX);
  EEPROM.put(CONFIG_SENSITIVITY_INDEX,(byte) DEFAULT_SENSITIVITY==SENSITIVITY_ONE_PERCENT?0:1);
}
/**
 * Load values from EEPROM.
 */
void onLoad(){
  byte voltage_index;
  byte sensitivity;
  EEPROM.get(CONFIG_VOLTAGE_LIMIT_INDEX,voltage_index);
  EEPROM.get(CONFIG_SENSITIVITY_INDEX,sensitivity);
  setSensitivity(sensitivity==0?SENSITIVITY_ONE_PERCENT:SENSITIVITY_FIVE_PERCENT);
  setVoltageLimitIndex(voltage_index);
}

/**
 * Check if it is the first time.
 */
void initConfiguration(){
  int val = EEPROM.read(0); // READ THE FIRST BYTE.
  firstTime = (val != CONFIG_FIRST_TIME_BYTE);   // CHECK FIRST TIME 
  if (firstTime==true){ // IT IS?
    EEPROM.write(0,CONFIG_FIRST_TIME_BYTE); // WRITE THE FIRST_BYTE IN THE EEPROM[0].
  }
}
/**
 * Load configuration or save if it is the first time.
 */
void loadConfiguration(){
  if (firstTime==true){
    onFirstTimeSave(); // save any default value.
  }else{ 
    onLoad();// load any value from eeprom
  }
  
}
/**
 * Save sensitivity
 */
void saveSensitivity(float value){
  EEPROM.put(CONFIG_SENSITIVITY_INDEX,value);
}
/**
 * Save voltage limit.
 */
void saveVoltageLimit(int value){
  EEPROM.put(CONFIG_VOLTAGE_LIMIT_INDEX,value);
}
