#ifndef sensitivity_H
#define sensitivity_H
#include "const.h"


/**
 * return the sensitivity
 */
float getSensitivity();
/**
 * change the sensitivity from:
 * SENSITIVITY_ONE_PERCENT => SENSITIVITY_FIVE_PERCENT
 * SENSITIVITY_FIVE_PERCENT => SENSITIVITY_ONE_PERCENT
 */
void changeSensitivity();

/**
 * set Sensitivity; 
 * mainly used from configuration module.
 */
void  setSensitivity(float sensitivity);

#endif
