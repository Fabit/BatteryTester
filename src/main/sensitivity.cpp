#include "sensitivity.h"

float sensitivity = DEFAULT_SENSITIVITY;


/**
 * change the sensitivity from:
 * SENSITIVITY_ONE_PERCENT => SENSITIVITY_FIVE_PERCENT
 * SENSITIVITY_FIVE_PERCENT => SENSITIVITY_ONE_PERCENT
 */
void changeSensitivity(){
  if (sensitivity==SENSITIVITY_ONE_PERCENT){
    sensitivity = SENSITIVITY_FIVE_PERCENT;
  }else{
    sensitivity = SENSITIVITY_ONE_PERCENT;
  }
}

 

/**
 * return the sensitivity
 */
float getSensitivity(){
  return sensitivity;
}
/**
 * set Sensitivity; 
 * mainly used from configuration module.
 */
void setSensitivity(float newsens){
  sensitivity = newsens;
}

