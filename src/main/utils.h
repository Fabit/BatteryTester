#ifndef utils_H
#define utils_H
/**
 * Check if value is inside the range
 * (in*(1-percentage),in*(1+percentage))
 */
bool checkRange(float value,float in,float percentage);

#endif
