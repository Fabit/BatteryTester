#ifndef voltageLevel_H
#define voltageLevel_H
#include "const.h"

/**
 * Next voltage limit
 */
bool   nextVoltageLimit();
/**
 * Prev voltage limit
 */
bool   prevVoltageLimit();
/**
 * Get the voltage limit
 */
float  getVoltageLimit();

/**
 * Set the next voltage limit index.
 * @param int new_voltindex 
 */
void setVoltageLimitIndex(int voltage_index);
/**
 * Retrieve the voltage limit
 */
int  getVoltageLimitIndex();
#endif
